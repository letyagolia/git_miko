﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Miko.Data.Dto
{
   public class PhotoForReturnDto
    {

        public Guid Id { get; set; }

        public string Url { get; set; }

        public DateTime DateAdded { get; set; }

        public bool IsMain { get; set; }

        public string PublicId { get; set; }
    }
}
