﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Miko.Data.Dto
{
    public class CreationPhotoDto //класс для отправки фотографии
    {
        public string Url { get; set; }

        public IFormFile File { get; set; }

        public DateTime DateAdded { get; set; }

        public string PublicId { get; set; }


        public CreationPhotoDto()
        {
            DateAdded = DateTime.Now;
        }
    }
}
