﻿using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Miko.Data.Dto
{
    public class UserDto
    {
        public Guid Id { get; set; }
   
        public string Email { get; set; }
        public string UserName { get; set; }   
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Password { get; set; }    
        public string Phone { get; set; }  
        public float LitersLastMonth { get; set; }
        public float LitersThisMonth { get; set; }

        public Guid? LoyaltyId { get; set; } 
        public LoyaltyDto Loyalty { get; set; }


        public float SavedMoney { get; set; }  // общая сэкономленная сумма 
     
       
        public List<TransactionDto> Transactions { get; set; } = new List<TransactionDto>();

        public List<ImageDto> Images { get; set; } = new List<ImageDto>();
    }
}
