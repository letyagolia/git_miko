﻿using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Text;

namespace Miko.Data.Dto
{
    public class ImageDto
    {
        public Guid Id { get; set; }

        public string Url { get; set; }

        public DateTime DateAdded { get; set; }

        public bool IsMain { get; set; }

        public string PublicId { get; set; }

        public User User { get; set; }

        public int UserId { get; set; }
    }
}
