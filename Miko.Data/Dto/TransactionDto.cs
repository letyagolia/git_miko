﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Miko.Data.Dto
{
    public class TransactionDto
    {
        public Guid Id { get; set; }
        public Guid? FuelId { get; set; }    //Название топлива
        public FuelDto Fuel { get; set; }
        public float Liters { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public Guid? GasStationId { get; set; }
        public GasStationDto GasStation { get; set; }
        public Guid? UserId { get; set; }
        public UserDto User { get; set; }
        public float Price { get; set; }
        public float SavedMoney { get; set; }
    }
}
