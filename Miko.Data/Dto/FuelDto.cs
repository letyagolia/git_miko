﻿using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Miko.Data.Dto
{
    public class FuelDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public float Price { get; set; }

        
        public List<TransactionDto> Transactions { get; set; } = new List<TransactionDto>();
    }
}