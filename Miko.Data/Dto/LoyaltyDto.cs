﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Miko.Data.Dto
{
    public class LoyaltyDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public float Liters { get; set; }
        public float Coef { get; set; }
        public List<UserDto> Users { get; set; } = new List<UserDto>();

    }
}
