﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Miko.Data.Dto
{
    public class GasStationDto
    {
        public Guid Id { get; set; }
        public string Address { get; set; }

       
        public List<TransactionDto> Transactions { get; set; } = new List<TransactionDto>();
    }
}
