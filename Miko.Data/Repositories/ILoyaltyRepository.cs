﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Data.Repositories
{
    public interface ILoyaltyRepository
    {
        Task<List<LoyaltyDto>> GetAllAsync();
        Task<LoyaltyDto> GetByIdAsync(Guid id);
        Task<LoyaltyDto> GetByTitleAsync(string title);
        Task<LoyaltyDto> GetByLitersAsync(float liters);
        Task<LoyaltyDto> CreateAsync(LoyaltyDto loyalty);
        Task<bool> DeleteAsync(Guid id);
        Task<bool> UpdateAsync(LoyaltyDto loyalty);
    }
}
