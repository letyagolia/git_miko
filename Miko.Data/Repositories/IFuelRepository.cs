﻿using Miko.Data.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Data.Repositories
{
    public interface IFuelRepository
    {
        Task<List<FuelDto>> GetAllAsync();
        Task<FuelDto> GetByIdAsync(Guid? id);
        Task<FuelDto> GetByTitleAsync(string title);
        Task<FuelDto> CreateAsync(FuelDto fuel);
        Task<bool> DeleteAsync(Guid id);
        Task<bool> UpdateAsync(FuelDto fuel);
    }
}
