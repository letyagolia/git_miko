﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Data.Repositories
{
    public interface IImgRepository
    {
        Task<PhotoForReturnDto> GetPhotoAsync(Guid id);
        Task<Imag> AddPhotoForUser(Guid userId, CreationPhotoDto photoForDto);
       
        //Task<bool> DeleteAsync(Guid id);
        //Task<bool> UpdateAsync(ImageDto image);
    }
}
