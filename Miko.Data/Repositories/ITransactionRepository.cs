﻿using Miko.Data.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Data.Repositories
{
    public interface ITransactionRepository
    {
        Task<List<TransactionDto>> GetAllAsync();
        Task<TransactionDto> GetByIdAsync(Guid id);
        Task<List<TransactionDto>> GetByUserIdAsync(Guid id);
        Task<TransactionDto> CreateAsync(TransactionDto transaction);
        Task<bool> DeleteAsync(Guid id);
        Task<bool> DeleteAllByYearAndMonthAsync(string year, string month);
        Task<bool> UpdateAsync(TransactionDto transaction);
    }
}
