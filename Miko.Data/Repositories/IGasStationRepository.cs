﻿using Miko.Data.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Data.Repositories
{
    public interface IGasStationRepository
    {
        Task<List<GasStationDto>> GetAllAsync();
        Task<GasStationDto> GetByIdAsync(Guid? id);
        Task<GasStationDto> CreateAsync(GasStationDto gas);
        Task<bool> UpdateAsync(GasStationDto gas);
        Task<bool> DeleteAsync(Guid id);
    }
}
