﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Data.Repositories
{
    public interface IUserRepository
    {
        Task<List<UserDto>> GetAllAsync();
        Task<UserDto> GetByIdAsync(Guid? id);
        Task<List<UserDto>> GetByLoyaltyIdAsync(Guid id);
        Task<UserDto> GetByPhoneAsync(string phone);
        Task<bool> UpdateAllUsersLoyalty(); // Обновить программу лояльности у всех пользователей
        Task<bool> SwapLitersEveryMonthAndUpdateLoyalties();//Из-за смены месяца передать в LitersLastMonth текущие литры, затем обнулить текущие литры
        Task<bool> DeductSavedMoney(Guid id, float money);
        Task<UserDto> CreateAsync(UserDto user);
        Task<bool> DeleteAsync(Guid id);
        Task<bool> UpdateAfterTransactionAsync(UserDto user);
        Task<bool> ChangePassword(string phone, string newPassword);
        Task<bool> ChangeEmail(Guid id, string newEmail);
        Task<bool> ChangePhone(Guid id, string newPhone);

    }
}
