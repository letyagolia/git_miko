﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miko.Data.Converters
{
    public static class ImageConverter
    {
        public static ImageDto Convert(CreationPhotoDto image)
        {
            return new ImageDto
            {
               
                Url = image.Url,
                DateAdded = image.DateAdded,
                
                PublicId = image.PublicId
               

            };
        }

        public static CreationPhotoDto Convert(ImageDto image)
        {
            return new CreationPhotoDto
            {
               
                Url = image.Url,
                DateAdded = image.DateAdded,
             
                PublicId = image.PublicId

            };
        }

        public static List<ImageDto> Convert(List<CreationPhotoDto> images)
        {
            return images.Select(a => Convert(a)).ToList();
        }

        public static List<CreationPhotoDto> Convert(List<ImageDto> images)
        {
            return images.Select(a => Convert(a)).ToList();
        }
    }
}
