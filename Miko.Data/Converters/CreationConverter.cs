﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miko.Data.Converters
{
    public class CreationConverter
    {
        public static Imag Convert(CreationPhotoDto image)
        {
            return new Imag
            {

               
                Url = image.Url,
                DateAdded = image.DateAdded,
                PublicId = image.PublicId



            };
        }

        public static CreationPhotoDto Convert(Imag image)
        {
            return new CreationPhotoDto
            {
                
                Url = image.Url,
                DateAdded = image.DateAdded,
                PublicId = image.PublicId

            };
        }

        public static List<Imag> Convert(List<CreationPhotoDto> images)
        {
            return images.Select(a => Convert(a)).ToList();
        }

        public static List<CreationPhotoDto> Convert(List<Imag> images)
        {
            return images.Select(a => Convert(a)).ToList();
        }
    }
}
