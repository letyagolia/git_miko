﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miko.Data.Converters
{
    public static class TransactionConverter
    {
        public static Transaction Convert(TransactionDto tr)
        {
            return new Transaction
            {
                Id = tr.Id,
                FuelId = tr.FuelId,
                //Fuel = FuelConverter.Convert(tr.Fuel),
                Liters = tr.Liters,
                Date = tr.Date,
                Time = tr.Time,
                GasStationId = tr.GasStationId,
                //GasStation = GasStationConverter.Convert(tr.GasStation),
                UserId = tr.UserId,
                //User = UserConverter.Convert(tr.User),
                Price = tr.Price,
                SavedMoney = tr.SavedMoney                
            };
        }
        public static TransactionDto Convert(Transaction tr)
        {
            if (tr.FuelId == null && tr.GasStationId == null && tr.UserId == null)
            {
                return new TransactionDto
                {
                    Id = tr.Id,
                    FuelId = tr.FuelId,
                    Liters = tr.Liters,
                    Date = tr.Date,
                    Time = tr.Time,
                    GasStationId = tr.GasStationId,
                    UserId = tr.UserId,
                    Price = tr.Price,
                    SavedMoney = tr.SavedMoney
                };
            }
            if (tr.UserId == null && tr.GasStationId == null)
            {
                return new TransactionDto
                {
                    Id = tr.Id,
                    FuelId = tr.FuelId,
                    Fuel = FuelConverter.Convert(tr.Fuel),
                    Liters = tr.Liters,
                    Date = tr.Date,
                    Time = tr.Time,
                    GasStationId = tr.GasStationId,
                    UserId = tr.UserId,
                    Price = tr.Price,
                    SavedMoney = tr.SavedMoney
                };
            }
            if (tr.FuelId == null && tr.GasStationId == null)
            {
                return new TransactionDto
                {
                    Id = tr.Id,
                    FuelId = tr.FuelId,
                    Liters = tr.Liters,
                    Date = tr.Date,
                    Time = tr.Time,
                    GasStationId = tr.GasStationId,
                    UserId = tr.UserId,
                    User = UserConverter.Convert(tr.User),
                    Price = tr.Price,
                    SavedMoney = tr.SavedMoney
                };
            }
            if (tr.FuelId == null && tr.UserId == null)
            {
                return new TransactionDto
                {
                    Id = tr.Id,
                    FuelId = tr.FuelId,
                    Liters = tr.Liters,
                    Date = tr.Date,
                    Time = tr.Time,
                    GasStationId = tr.GasStationId,
                    GasStation = GasStationConverter.Convert(tr.GasStation),
                    UserId = tr.UserId,
                    Price = tr.Price,
                    SavedMoney = tr.SavedMoney
                };
            }
            if (tr.FuelId == null) {
                return new TransactionDto
                {
                    Id = tr.Id,
                    FuelId = tr.FuelId,
                    Liters = tr.Liters,
                    Date = tr.Date,
                    Time = tr.Time,
                    GasStationId = tr.GasStationId,
                    GasStation = GasStationConverter.Convert(tr.GasStation),
                    UserId = tr.UserId,
                    User = UserConverter.Convert(tr.User),
                    Price = tr.Price,
                    SavedMoney = tr.SavedMoney
                };
            }
            if (tr.GasStationId == null) {
                return new TransactionDto
                {
                    Id = tr.Id,
                    FuelId = tr.FuelId,
                    Fuel = FuelConverter.Convert(tr.Fuel),
                    Liters = tr.Liters,
                    Date = tr.Date,
                    Time = tr.Time,
                    GasStationId = tr.GasStationId,
                    UserId = tr.UserId,
                    User = UserConverter.Convert(tr.User),
                    Price = tr.Price,
                    SavedMoney = tr.SavedMoney
                };
            }
            if (tr.UserId == null || tr.User == null) {
                return new TransactionDto
                {
                    Id = tr.Id,
                    FuelId = tr.FuelId,
                    Fuel = FuelConverter.Convert(tr.Fuel),
                    Liters = tr.Liters,
                    Date = tr.Date,
                    Time = tr.Time,
                    GasStationId = tr.GasStationId,
                    GasStation = GasStationConverter.Convert(tr.GasStation),
                    UserId = tr.UserId,
                    Price = tr.Price,
                    SavedMoney = tr.SavedMoney
                };
            }
            
            else
            {
                return new TransactionDto
                {
                    Id = tr.Id,
                    FuelId = tr.FuelId,
                    Fuel = FuelConverter.Convert(tr.Fuel),
                    Liters = tr.Liters,
                    Date = tr.Date,
                    Time = tr.Time,
                    GasStationId = tr.GasStationId,
                    GasStation = GasStationConverter.Convert(tr.GasStation),
                    UserId = tr.UserId,
                    User = UserConverter.Convert(tr.User),
                    Price = tr.Price,
                    SavedMoney = tr.SavedMoney
                };
            }
            
        }
        public static List<Transaction> Convert(List<TransactionDto> trans)
        {
            return trans.Select(a => Convert(a)).ToList();
        }
        public static List<TransactionDto> Convert(List<Transaction> trans)
        {
            return trans.Select(a => Convert(a)).ToList();
        }
         
    }
}
