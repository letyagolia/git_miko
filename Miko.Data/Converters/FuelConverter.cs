﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miko.Data.Converters
{
    public static class FuelConverter
    {
        public static Fuel Convert(FuelDto fuel)
        {
           return new Fuel
            {
                Id = fuel.Id,
                Title = fuel.Title,
                Price = fuel.Price                    
            };
        }
        public static FuelDto Convert(Fuel fuel)
        {
            return new FuelDto
            {
                Id = fuel.Id,
                Title = fuel.Title,
                Price = fuel.Price,
            };
        }
        public static List<Fuel> Convert(List<FuelDto> fuels)
        {
            return fuels.Select(a => Convert(a)).ToList(); 
        }
        public static List<FuelDto> Convert(List<Fuel> fuels)
        {
            return fuels.Select(a => Convert(a)).ToList();
        }
    }
}
