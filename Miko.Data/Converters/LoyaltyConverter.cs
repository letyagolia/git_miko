﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miko.Data.Converters
{
    public static class LoyaltyConverter
    {
        public static Loyalty Convert(LoyaltyDto loyalty)
        {
            return new Loyalty
            {
                Id = loyalty.Id,
                Title = loyalty.Title,
                Liters = loyalty.Liters,
                Coef = loyalty.Coef,
            };
        }
        public static LoyaltyDto Convert(Loyalty loyalty)
        {
            return new LoyaltyDto
            {
                Id = loyalty.Id,
                Title = loyalty.Title,
                Liters = loyalty.Liters,
                Coef = loyalty.Coef,
            };
        }
        public static List<Loyalty> Convert(List<LoyaltyDto> loyalties)
        {
            return loyalties.Select(a => Convert(a)).ToList();
        }
        public static List<LoyaltyDto> Convert(List<Loyalty> loyalties)
        {
            return loyalties.Select(a => Convert(a)).ToList();
        }
    }
}
