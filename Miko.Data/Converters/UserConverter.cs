﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miko.Data.Converters
{
    public static class UserConverter
    {
        public static User Convert(UserDto us)
        {
            return new User
            {
                Id = us.Id,
                Email = us.Email,
                UserName = us.Phone,
                Name = us.Name,
                Surname = us.Surname,
                Password = us.Password,
                PhoneNumber = us.Phone,
                LitersLastMonth = us.LitersLastMonth,
                LitersThisMonth = us.LitersThisMonth,
                LoyaltyId = us.LoyaltyId,
                SavedMoney = us.SavedMoney,
                //Loyalty = LoyaltyConverter.Convert(us.Loyalty),
            };
        }
        public static UserDto Convert(User us)
        {
            if (us.LoyaltyId == null || us.Loyalty == null)
            {
                return new UserDto
                {
                    Id = us.Id,
                    Email = us.Email,
                    UserName = us.PhoneNumber,
                    Name = us.Name,
                    Surname = us.Surname,
                    Password = us.PasswordHash,
                    Phone = us.PhoneNumber,
                    //Barcode = us.Barcode,
                    //CardNum = us.CardNum,
                    LitersLastMonth = us.LitersLastMonth,
                    LitersThisMonth = us.LitersThisMonth,
                    LoyaltyId = us.LoyaltyId,
                    SavedMoney = us.SavedMoney                    
                };
            }
            return new UserDto
            {
                Id = us.Id,
                Email = us.Email,
                UserName = us.PhoneNumber,
                Name = us.Name,
                Surname = us.Surname,
                Password = us.PasswordHash,
                Phone = us.PhoneNumber,
                //Barcode = us.Barcode,
                //CardNum = us.CardNum,
                LitersLastMonth = us.LitersLastMonth,
                LitersThisMonth = us.LitersThisMonth,
                LoyaltyId = us.LoyaltyId,
                SavedMoney = us.SavedMoney,
                Loyalty = LoyaltyConverter.Convert(us.Loyalty),
            };
        }
        public static List<User> Convert(List<UserDto> users)
        {
            return users.Select(a => Convert(a)).ToList();
        }
        public static List<UserDto> Convert(List<User> users)
        {
            return users.Select(a => Convert(a)).ToList();
        }
    }
}
