﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miko.Data.Converters
{
    public class ReturnConverter
    {
        public static PhotoForReturnDto Convert(Imag image)
        {
            return new PhotoForReturnDto
            {
                Id = image.Id,
                Url = image.Url,
                DateAdded = image.DateAdded,
                IsMain = image.IsMain,
                PublicId = image.PublicId


            };
        }

        public static Imag Convert(PhotoForReturnDto image)
        {
            return new Imag
            {

                Id = image.Id,
                Url = image.Url,
                DateAdded = image.DateAdded,
                IsMain = image.IsMain,
                PublicId = image.PublicId

            };
        }

        public static List<PhotoForReturnDto> Convert(List<Imag> images)
        {
            return images.Select(a => Convert(a)).ToList();
        }

        public static List<Imag> Convert(List<PhotoForReturnDto> images)
        {
            return images.Select(a => Convert(a)).ToList();
        }
    }
}
