﻿using Miko.Data.Dto;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miko.Data.Converters
{
    public static class GasStationConverter
    {
        public static GasStation Convert(GasStationDto gas)
        {
            return new GasStation
            {
                Id = gas.Id,
                Address = gas.Address,
            };
        }
        public static GasStationDto Convert(GasStation gas)
        {
            return new GasStationDto
            {
                Id = gas.Id,
                Address = gas.Address,
            };
        }
        public static List<GasStation> Convert(List<GasStationDto> gass)
        {
            return gass.Select(a => Convert(a)).ToList();
        }
        public static List<GasStationDto> Convert(List<GasStation> gass)
        {
            return gass.Select(a => Convert(a)).ToList();
        }
    }
}
