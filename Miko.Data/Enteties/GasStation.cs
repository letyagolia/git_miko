﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Miko.Data.Enteties
{
    public class GasStation
    {
        public Guid Id { get; set; }
        public string Address { get; set; }

        
        public List<Transaction> Transactions { get; set; } = new List<Transaction>();
    }
}
