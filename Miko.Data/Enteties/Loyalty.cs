﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Miko.Data.Enteties
{
    public class Loyalty
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public float Liters { get; set; }
        public float Coef { get; set; }

        
        public List<User> Users { get; set; } = new List<User>();
    }
}
