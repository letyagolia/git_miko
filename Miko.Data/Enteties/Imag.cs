﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Miko.Data.Enteties
{
    public class Imag
    {
        public Guid Id { get; set; }

        public string Url { get; set; }

        public DateTime DateAdded { get; set; }

        public bool IsMain { get; set; } // Масштабируемость авы

        public string PublicId { get; set; } // Cloudinary 

        public User User { get; set; }

        public Guid? UserId { get; set; }
    }
}
