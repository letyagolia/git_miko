﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Miko.Data.Enteties
{
    public class User:UserIde
    {
        public string Password { get; set; } 
        public string Phone { get; set; }   
        public float LitersLastMonth { get; set; }
        public float LitersThisMonth { get; set; }
        public Guid? LoyaltyId { get; set; } //уровень лояльности
        public Loyalty Loyalty { get; set; }
        public float SavedMoney { get; set; }  // общая сэкономленная сумма 
       
        public List<Transaction> Transactions { get; set; } = new List<Transaction>();

        public List<Imag> Images { get; set; } = new List<Imag>();
    }
}
