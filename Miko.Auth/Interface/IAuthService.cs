﻿using Miko.Data.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Auth.Interface
{
    public interface IAuthService
    {
        Task<object> Login(string phone, string password);

        Task<object> Register(UserDto item);
    }
}
