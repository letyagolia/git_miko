﻿using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Auth.Interface
{
    public interface IJwtGenerator
    {
        Task<object> GenerateJwt(User user);
    }
}
