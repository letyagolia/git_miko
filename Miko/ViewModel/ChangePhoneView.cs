﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Miko.ViewModel
{
    public class ChangePhoneView
    {
        public string UserName { get; set; }
        public string NewPhone { get; set; }
    }
}
