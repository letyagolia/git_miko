﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Miko.ViewModel
{
    public class ChangePswView
    {
        public string Phone { get; set; }
        public string NewPassword { get; set; }
    }
}
