﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Miko.ViewModel
{
    public class ChangeEmailView
    {
        public string UserName { get; set; }
        public string NewEmail { get; set; }
    }
}
