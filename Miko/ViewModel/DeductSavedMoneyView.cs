﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Miko.ViewModel
{
    public class DeductSavedMoneyView
    {
        public Guid Id { get; set; }
        public float Money { get; set; }
    }
}
