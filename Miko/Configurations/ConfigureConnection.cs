﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Miko.Core.EF;
using Miko.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Miko.Configurations
{
    public static class ConfigureConnection
    {
        public static IServiceCollection AddConnectionProvider(
            this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddDbContext<MicoContext>(opt => opt.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly("Miko"))
                .EnableSensitiveDataLogging()
            );
            services.
                Configure<CloudinarySettings>(configuration.GetSection("CloudinarySettings"));  

            return services;
        }
    }
}
