﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Miko.Auth.Interface;
using Miko.Auth.Services;
using Miko.Core.EF;
using Miko.Core.Helpers;
using Miko.Core.Repositories;
using Miko.Core.Services;
using Miko.Data.Enteties;
using Miko.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Miko.Configurations
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddIdentity(
           this IServiceCollection services)
        {
            services.AddIdentity<User, IdentityRole<Guid>>(o =>
            {
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireLowercase = false;
            })
                .AddEntityFrameworkStores<MicoContext>();

            return services;
        }

        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services
                .AddTransient<ITransactionRepository, TransactionRepository>()
                .AddTransient<IUserRepository, EnableSensitiveDataLogging>()
                .AddTransient<ILoyaltyRepository, LoyaltyRepository>()
                .AddTransient<IFuelRepository, FuelRepository>()
                .AddTransient<IJwtGenerator, JwtGenerator>()
                .AddTransient<IAuthService, AuthService>()
                .AddTransient<IImgRepository, ImgRepository>()
               

                .AddTransient<IGasStationRepository, GasStationRepository>();

            return services;
        }

   

        public static IServiceCollection AddCorsConfiguration(this IServiceCollection services) =>
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", new Microsoft.AspNetCore.Cors.Infrastructure.CorsPolicyBuilder()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin()
                    .AllowCredentials()
                    .Build());
            });


    }

}

