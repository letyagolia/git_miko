﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Miko.Auth.Interface;
using Miko.Data.Dto;
using Miko.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Miko.Controllers
{
    [Route("miko/[action]")]
    public class AuthController: Controller
    {
        private readonly IAuthService _auth;


        public AuthController(IAuthService auth)
        {
            _auth = auth;
        }

        [HttpPost]                          
        [AllowAnonymous]                                                       
        [Produces(typeof(object))]
        public async Task<ActionResult<object>> Login([FromBody] LoginViewModel form)
        {
            try
            {
                var result = await _auth.Login(form.Phone, form.Password);
                if (result == null)
                    return BadRequest();
                return result;
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]                                        //POST http://localhost:5000/api/register
        [AllowAnonymous]                            		/* FromeBody
                                                            "email": "nasbor31@yandex.ru",
	                                                        "name": "Анастасия",
	                                                        "phone": "89194107000", (именно так, вот так +7(929)892-99-32 не зайдет)
	                                                        "password": "1oilmikoil45" 
                                                            пароль для администратора содержит в себе "oilmikoil" без этого - обычный изер */
        [Produces(typeof(object))]
        public async Task<ActionResult<object>> Register([FromBody] UserDto item)
        {
            try
            {
                var result = await _auth.Register(item);
                if (result == null)
                    return BadRequest();
                return result;
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }

        }
    }
}
