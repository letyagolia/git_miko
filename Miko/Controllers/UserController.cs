﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Miko.Data.Dto;
using Miko.Data.Repositories;
using Miko.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Miko.Controllers
{
    [Route("miko/[controller]")]

    public class UserController : Controller
    {
        private readonly IUserRepository _repo;
        public UserController(IUserRepository repo)
        {
            _repo = repo;
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                return Ok(await _repo.GetAllAsync());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [Authorize(Roles = "admin, user")]
        [HttpGet("id/{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByIdAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [Authorize(Roles = "admin")]
        [HttpGet("loyaltyId/{id}")]
        public async Task<IActionResult> GetByLoyaltyId(Guid id)
        {
            try
            {
                return Ok(await _repo.GetByLoyaltyIdAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [Authorize (Roles = "admin, user")]
        [HttpGet("phone/{phone}")]
        public async Task<IActionResult> GetByPhoneAsync(string phone)
        {
            try
            {
                return Ok(await _repo.GetByPhoneAsync(phone));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserDto item)
        {
            try
            {
                return Ok(await _repo.CreateAsync(item));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        
        [HttpPut] 
        public async Task<IActionResult> Put([FromBody] UserDto item)
        {
            try
            {
                return Ok(await _repo.UpdateAfterTransactionAsync(item));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [Authorize(Roles = "admin, user")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                return Ok(await _repo.DeleteAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        
        [HttpGet("UpdateLoyalties")]
        public async Task<IActionResult> UpdateLoyalty()
        {
            try
            {
                return Ok(await _repo.UpdateAllUsersLoyalty());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet("SwapLiters")]
        public async Task<IActionResult> SwapLiters()
        {
            try
            {
                return Ok(await _repo.SwapLitersEveryMonthAndUpdateLoyalties());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        

        [HttpPost("SavedMoney")] 
        public async Task<IActionResult> DeductSavedMoney([FromBody] DeductSavedMoneyView form)
        {
            try
            {
                return Ok(await _repo.DeductSavedMoney(form.Id, form.Money));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

       
        [HttpPost("changePsw")]
        public async Task<IActionResult> ChangePasswordAsync([FromBody] ChangePswView form)
        {
            try
            {
                return Ok(await _repo.ChangePassword(form.Phone, form.NewPassword));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [Authorize(Roles = "admin, user")]
        [HttpPost("changeEmail/{id}&{newEmail}")]
        public async Task<IActionResult> ChangeEmail(Guid id, string newEmail)
        {
            try
            {
                return Ok(await _repo.ChangeEmail(id, newEmail));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [Authorize(Roles = "admin, user")]
        [HttpPost("changePhone/{id}&{newPhone}")]
        public async Task<IActionResult> ChangePhone(Guid id, string newPhone)
        {
            try
            {
                return Ok(await _repo.ChangePhone(id, newPhone));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
