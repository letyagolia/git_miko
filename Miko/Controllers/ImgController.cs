﻿using Microsoft.AspNetCore.Mvc;
using Miko.Data.Dto;
using Miko.Data.Enteties;
using Miko.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Miko.Data.Converters;
using Microsoft.AspNetCore.Authorization;

namespace Miko.Controllers
{
    [Authorize]
    [Route("miko/users/{userId}/photos")]
    public class ImgController : Controller
    {
        private readonly IImgRepository _repo;
      
    
     
     public ImgController(IImgRepository repo)
        {
            _repo = repo;
            
        }


        [Authorize(Roles = "admin, user")]
        [HttpGet("{id}", Name = "GetPhotoAsync")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                return Ok(await _repo.GetPhotoAsync(id));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [Authorize(Roles = "admin, user")]
        [HttpPost]
        public async Task<IActionResult> Post([FromForm] CreationPhotoDto photoForDto, Guid userId)
        {
            try
            {
                var photo = await _repo.AddPhotoForUser(userId, photoForDto);
               
                var photoReturn = ReturnConverter.Convert(photo);
               return CreatedAtRoute("GetPhotoAsync", new { userId, id = photo.Id }, photoReturn);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
