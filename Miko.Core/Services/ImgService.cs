﻿using Microsoft.AspNetCore.Http;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using Miko.Data.Dto;

namespace Miko.Core.Services
{
    public class ImgService
    {
        
        public static string UploadToServere(IFormFile file)
        {
            byte[] img = null;

            using (var binaryReader = new BinaryReader(file.OpenReadStream()))
            {
                img = binaryReader.ReadBytes((int)file.Length);
            }

            var str = Encoding.Unicode.GetString(img);

            string path = Guid.NewGuid().ToString() + ".txt";
            string PathDir = Directory.GetCurrentDirectory() + @"\Content";

            DirectoryInfo NewDir = new DirectoryInfo(PathDir);
            if (!NewDir.Exists)
            {
                NewDir.Create();
            }
           using (var fileStream = new FileStream(Directory.GetCurrentDirectory() + @"\Content\" + path, FileMode.Create))
            {

                fileStream.WriteAsync(img, 0, img.Length);
                fileStream.FlushAsync();

            }
            string result = Directory.GetCurrentDirectory() + @"\Content\" + path;

            return result;

        }

        public static string ImageReturn(string p)
        {
            string s = File.ReadAllText(p);
            string path = @"\Content\" + Guid.NewGuid().ToString() + ".jpg";
            byte[] imgb =  Convert.FromBase64String(s);
            using (var ms = new MemoryStream(imgb, 0, imgb.Length))
           {
                ms.Write(imgb, 0, imgb.Length);
                Image image = Image.FromStream(ms, true);
                image.Save(path);
           }
           

            return path;
        }
    }
}
