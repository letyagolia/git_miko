﻿using Microsoft.EntityFrameworkCore;
using Miko.Core.EF;
using Miko.Data.Converters;
using Miko.Data.Dto;
using Miko.Data.Enteties;
using Miko.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Core.Repositories
{
    public class LoyaltyRepository: ILoyaltyRepository
    {
        private readonly MicoContext _context;
       
        public LoyaltyRepository(MicoContext context)
        {
            _context = context;
        }
      
        public async Task<List<LoyaltyDto>> GetAllAsync()
        {
            return LoyaltyConverter.Convert(await _context.Loyalties.ToListAsync());
        }
        public async Task<LoyaltyDto> GetByIdAsync(Guid id)
        {
            var l = await _context.Loyalties.FindAsync(id);
            if (l == null) { return null; }
            return LoyaltyConverter.Convert(l);
        }

        public async Task<LoyaltyDto> GetByTitleAsync(string title)
        {
            var ls = await _context.Loyalties
                .FirstOrDefaultAsync(c => c.Title == title);
            if (ls == null) { return null; }
            return LoyaltyConverter.Convert(ls);
        }
        public async Task<LoyaltyDto> GetByLitersAsync(float liters)
        {
            var loyals = LoyaltyConverter.Convert(await _context.Loyalties.ToListAsync());
            if (loyals.Count == 0) { return null; }
            //Упорядочиваем лист loyals по возрастанию liters
            LoyaltyDto doplo;
            int count = loyals.Count;
            for (int i = 0; i < count; i++)
            {
                for (int j = i + 1; j < count; j++)
                    if (loyals[i].Liters > loyals[j].Liters)
                    {
                        doplo = loyals[i];
                        loyals[i] = loyals[j];
                        loyals[j] = doplo;
                    }
            }
            count = 0;
            if (liters < loyals[0].Liters) { return null; }
            //Проверяем уровень лояльности
            for (int k = 0; k < loyals.Count; k++)
            {
                if (liters >= loyals[k].Liters) { count++; continue; }
                else { count--; return loyals[count]; }
            }
            return loyals[count-1];
        }

        public async Task<LoyaltyDto> CreateAsync(LoyaltyDto loyalty)
        {
            var loyal = _context.Loyalties.Add(LoyaltyConverter.Convert(loyalty));
            await _context.SaveChangesAsync();
            return LoyaltyConverter.Convert(loyal.Entity);

        }
        public async Task<bool> DeleteAsync(Guid id)
        {
            var loyal = await _context.Loyalties.FindAsync(id);
            if (loyal == null)
                return false;
            await _context.Users.Where(c => c.LoyaltyId == id).ToListAsync();
            _context.Loyalties.Remove(loyal);
            await _context.SaveChangesAsync();
            return true;
        }
        public async Task<bool> UpdateAsync(LoyaltyDto loyalty)
        {
            if (loyalty == null)
                return false;
            _context.Loyalties.Update(LoyaltyConverter.Convert(loyalty));
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
