﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Miko.Core.EF;
using Miko.Data.Converters;
using Miko.Data.Dto;
using Miko.Data.Enteties;
using Miko.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Core.Repositories
{
    public class EnableSensitiveDataLogging: IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly ILoyaltyRepository _lo;
        private readonly MicoContext _context;
        

        public EnableSensitiveDataLogging(MicoContext context, UserManager<User> um, ILoyaltyRepository lo)
        {
            _userManager = um;
            _lo = lo;
            _context = context;
            
        }

        public async Task<List<UserDto>> GetAllAsync()
        {
            return UserConverter.Convert(await _userManager.Users.Include(c=>c.Loyalty)
                .ToListAsync());
        }

        public async Task<UserDto> GetByIdAsync(Guid? id)
        {
            var user = await _userManager.Users.Include(c=>c.Loyalty).FirstOrDefaultAsync(c => c.Id == id);
            if (user == null) { return null; }
            return UserConverter.Convert(user);
        }
        public async Task<List<UserDto>> GetByLoyaltyIdAsync(Guid id)
        {
            var us = await _userManager.Users
                .Include(c => c.Loyalty)
                .Where(c => c.LoyaltyId == id).ToListAsync();
            if (us == null) { return null; }
            return UserConverter.Convert(us);
        }
        public async Task<UserDto> GetByPhoneAsync(string phone)
        {
            var us = await _userManager.Users.Include(c => c.Loyalty).FirstOrDefaultAsync(c => c.PhoneNumber == phone);
            if (us == null)
            {
                return null;
            }
            return UserConverter.Convert(us);
        }

        public async Task<UserDto> CreateAsync(UserDto item)
        {
            //Определим уровень лояльности
            item.LitersLastMonth = 0;
            item.LitersThisMonth = 0;
            item.Loyalty = await _lo.GetByLitersAsync(item.LitersLastMonth);
            item.LoyaltyId = item.Loyalty.Id;
            User user = UserConverter.Convert(item);
            var result = await _userManager.CreateAsync(user);
            if (!result.Succeeded)
                return null;

            var r = await _userManager.AddToRoleAsync(user, "user");
            if (!r.Succeeded)
                return null;

            return UserConverter.Convert(
                await _userManager.FindByEmailAsync(item.UserName));
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var us = await _userManager.FindByIdAsync(id.ToString());
            if (us == null)
                return false;
            await _context.Transactions.Where(c => c.UserId == id).ToListAsync();
            await _userManager.DeleteAsync(us);
            return true;
        }

        public async Task<bool> UpdateAfterTransactionAsync(UserDto item)
        {
            if (item == null)
            {
                return false;
            }
            var us = _context.Users.Find(item.Id);

            _context.Users.Attach(us);

            us.LitersThisMonth = item.LitersThisMonth;
            us.SavedMoney = item.SavedMoney;

            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateAllUsersLoyalty()
        {
            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            var u = await _context.Users.ToListAsync();
            if (u.Count == 0) { return false; }
            var users = UserConverter.Convert(u);
            foreach (UserDto us in users)
            {
                var us1 = _context.Users.Find(us.Id);
                _context.Users.Attach(us1);
                var us2 = UserConverter.Convert(us1);

                _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                us2.Loyalty = await _lo.GetByLitersAsync(us2.LitersLastMonth);
                if (us2.Loyalty == null) { continue; }
                us2.LoyaltyId = us2.Loyalty.Id;

                await _context.SaveChangesAsync();
            }
            return true;
        }
        public async Task<bool> SwapLitersEveryMonthAndUpdateLoyalties()
        {
            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var u = await _userManager.Users.ToListAsync();
            if (u.Count == 0) { return false; }
            foreach (var us in u)
            {
                _context.Users.Attach(us);

                var us1 = UserConverter.Convert(us);
                us.LitersLastMonth = us1.LitersThisMonth;
                us.LitersThisMonth = 0;

                _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                us1.Loyalty =  await _lo.GetByLitersAsync(us.LitersLastMonth);
                if (us1.Loyalty != null) { us.LoyaltyId = us1.Loyalty.Id; }
                //else { us.LoyaltyId = Guid.Empty; }

                await _context.SaveChangesAsync();
            }
            return true;
        }
        public async Task<bool> DeductSavedMoney(Guid id, float money)
        {
            if (id == Guid.Empty || money == 0) { return false; }
            var user = await _context.Users.FindAsync(id);
            _context.Users.Attach(user);
            user.SavedMoney -= money;
            if (user.SavedMoney < 0) { return false; }
            await _context.SaveChangesAsync();
            return true;
        }
        public async Task<bool> ChangePassword(string phone, string newPassword)
        {
            var us = await _userManager.FindByNameAsync(phone);
            if (us == null)
            {
                return false;
            }
            await _userManager.RemovePasswordAsync(us);
            IdentityResult result =  await _userManager.AddPasswordAsync(us, newPassword);
            if (!result.Succeeded)
            {
                return false;
            }
            return true;
        }
        public async Task<bool> ChangeEmail(Guid id, string newEmail)
        {
            var us = await _userManager.FindByIdAsync(id.ToString());
            if (us == null)
            {
                return false;
            }
            us.Email = newEmail;
            us.UserName = newEmail;
            IdentityResult result = await _userManager.UpdateAsync(us);
            if (!result.Succeeded)
            {
                return false;
            }
            return true;
        }
        public async Task<bool> ChangePhone(Guid id, string newPhone)
        {
            var us = await _userManager.FindByIdAsync(id.ToString());
            if (us == null)
            {
                return false;
            }
            
            IdentityResult result = await _userManager.SetPhoneNumberAsync(us, newPhone);
            if (!result.Succeeded)
            {
                return false;
            }
            return true;
        }
    }
}
