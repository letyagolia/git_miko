﻿
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Options;
using Miko.Core.EF;
using Miko.Core.Helpers;
using Miko.Core.Services;
using Miko.Data.Converters;
using Miko.Data.Dto;
using Miko.Data.Enteties;
using Miko.Data.Repositories;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Core.Repositories
{   
    
    public class ImgRepository: IImgRepository
    {
        /*
        private readonly MicoContext _context;

        public ImgRepository(MicoContext context)
        {
            _context = context; 
        }

        public async Task<List<ImageDto>> GetAllAsync()
        {
            var result = ImageConverter.Convert(await _context.Images.ToListAsync());

            foreach( ImageDto m in result)
            {
                m.Path = ImgService.ImageReturn(m.Path);
                _context.Images.Update(ImageConverter.Convert(m));

            }
            await _context.SaveChangesAsync();
            var r = ImageConverter.Convert(await _context.Images.ToListAsync());

            return r;

        }

        public async Task<ImageDto> GetByPathAsync(Guid id)
        {
           var img = ImageConverter.Convert(await _context.Images.FindAsync(id));                     
            img.Path = ImgService.ImageReturn(img.Path);
           
            _context.Images.Update(ImageConverter.Convert(img));
            await _context.SaveChangesAsync();
            var image = ImageConverter.Convert(await _context.Images.FindAsync(img.Id));
            
            return image;  
        }

        public async Task<ImageDto> CreateAsync(CreatePost item)
        {
            var post = new ImageDto();

            if (item.MyImage != null)
            {
                post.Path = ImgService.UploadToServere(item.MyImage);
            }


            var img = _context.Images.Add(ImageConverter.Convert(post));
            await _context.SaveChangesAsync();

            return ImageConverter.Convert(img.Entity); // img.Entity
        } */

        //-----------------------------------------------------------------------
        //                   НОВОЕ

        private readonly MicoContext _context;
        
        private readonly UserManager<User> _userManager;
        private readonly IOptions<CloudinarySettings> _cloudinaryConfig;
        private Cloudinary _cloudinary;

        public ImgRepository(MicoContext context, UserManager<User> um,
            IOptions<CloudinarySettings> cloudinaryConfig)
        {
            _context = context;
            
            _userManager = um;

            _cloudinaryConfig = cloudinaryConfig; // подключение облака
            Account acc = new Account(
                _cloudinaryConfig.Value.CloudName,
                _cloudinaryConfig.Value.ApiKey,
                _cloudinaryConfig.Value.ApiSecret
            );
            _cloudinary = new Cloudinary(acc);
        }

        public async Task<PhotoForReturnDto> GetPhotoAsync (Guid id)
        {
            var photo = await _context.Images.FirstOrDefaultAsync(p => p.Id == id);

            var result = ReturnConverter.Convert(photo);

            return result;
        }
        
        public async Task<Imag> AddPhotoForUser (Guid userId,
            CreationPhotoDto photoForDto)
        {
            var user = await _userManager.Users
                .Include(c => c.Loyalty).FirstOrDefaultAsync(c => c.Id == userId);

            var file = photoForDto.File;

            var uploadResult = new ImageUploadResult(); // облачный класс  

            if (file.Length > 0)
            {
                using (var stream = file.OpenReadStream())
                {
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(file.Name, stream),
                        Transformation = new Transformation()
                        .Width(500).Height(500).Crop("fill").Gravity("face")
                    };

                    uploadResult = _cloudinary.Upload(uploadParams); // загрузка в облако

                }
            }

            photoForDto.Url = uploadResult.Uri.ToString();
            photoForDto.PublicId = uploadResult.PublicId; // вот это конвертируем в класс Image

           var photo = CreationConverter.Convert(photoForDto); //здесь

            if (!user.Images.Any(u => u.IsMain))
                photo.IsMain = true;

            user.Images.Add(photo);
      
           await _context.SaveChangesAsync();

            return photo;
           
        }
    }
}
