﻿using Microsoft.EntityFrameworkCore;
using Miko.Core.EF;
using Miko.Data.Converters;
using Miko.Data.Dto;
using Miko.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Core.Repositories
{
    public class TransactionRepository: ITransactionRepository
    {
        private readonly MicoContext _context;
        private readonly IUserRepository _user;
        private readonly IFuelRepository _fuel;
        private readonly ILoyaltyRepository _lo;
        private readonly IGasStationRepository _gs;

        public TransactionRepository(MicoContext context, IUserRepository user, IFuelRepository fuel, ILoyaltyRepository lo, IGasStationRepository gs)
        {
            _context = context;
            _fuel = fuel;
            _user = user;
            _lo = lo;
            _gs = gs;

        }
        public async Task<List<TransactionDto>> GetAllAsync()
        {
            return TransactionConverter.Convert(await _context.Transactions
                .Include(c => c.Fuel)
                .Include(c=>c.GasStation)
                .ToListAsync());
        }
        public async Task<TransactionDto> GetByIdAsync(Guid id)
        {
            var tr = await _context.Transactions
                .Include(c => c.Fuel)
                .Include(c => c.GasStation)
                .FirstOrDefaultAsync(c => c.Id == id);
            if (tr == null) { return null; }
            return TransactionConverter.Convert(tr);
        }
        public async Task<List<TransactionDto>> GetByUserIdAsync(Guid id)
        {
            var tr = await _context.Transactions
                .Include(c => c.Fuel)
                .Include(c => c.GasStation)
                .Where(c => c.UserId == id)
                .ToListAsync();
            if (tr == null) { return null; }
            return TransactionConverter.Convert(tr);
        }
        public async Task<TransactionDto> CreateAsync(TransactionDto transaction)
        {
            if (transaction.FuelId == Guid.Empty || transaction.UserId == Guid.Empty || transaction.GasStationId == Guid.Empty)
            {
                return null;
            }
            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            //Дополняем сущность Транзакция дополнительными сущностями Fuel & User & GasStation
            transaction.Fuel = await _fuel.GetByIdAsync(transaction.FuelId);
            transaction.User = await _user.GetByIdAsync(transaction.UserId);
            transaction.GasStation = await _gs.GetByIdAsync(transaction.GasStationId);
            transaction.Date = DateTime.Now.ToShortDateString();
            transaction.Time = DateTime.Now.ToLongTimeString();

            if (transaction.Fuel == null || transaction.User == null || transaction.GasStation == null)
            {
                return null;
            }

            transaction.Price = transaction.Liters * transaction.Fuel.Price;
            if (transaction.User.Loyalty != null)
            {
                transaction.SavedMoney = transaction.Liters * transaction.User.Loyalty.Coef;
            }
            else
            {
                transaction.SavedMoney = 0;
            }
                
            
            var tr = _context.Transactions.Add(TransactionConverter.Convert(transaction));
            await _context.SaveChangesAsync();

            //Передаем SavedMoney & Liters в сущность соответствующего юзера
            var user = transaction.User;
            user.SavedMoney += transaction.SavedMoney;
            user.LitersThisMonth += transaction.Liters;
            await _user.UpdateAfterTransactionAsync(user);

            return TransactionConverter.Convert(await _context.Transactions.Include(c => c.Fuel)
                .Include(c => c.GasStation)
                .Include(c => c.User)
                    .ThenInclude(h => h.Loyalty)
                .FirstOrDefaultAsync(c => c.Id == tr.Entity.Id)); 
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var tr = await _context.Transactions.FindAsync(id);
            if (tr == null)
                return false;
            tr.User = _context.Users.Include(c => c.Loyalty).FirstOrDefault(c => c.Id == tr.UserId);
            if (tr.User == null) 
            {
                _context.Transactions.Remove(tr);
                await _context.SaveChangesAsync();
                return true; 
            }
            _context.Transactions.Remove(tr);
            await _context.SaveChangesAsync();

            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            //Удаляем SavedMoney & Liters этой транзакции у User
            var us = tr.User; 
            us.SavedMoney -= tr.SavedMoney;
            us.LitersThisMonth -= tr.Liters;

            if (us.SavedMoney < 0) { us.SavedMoney = 0; }
            if (us.LitersThisMonth < 0) { us.LitersThisMonth = 0; }

            
            _context.Users.Update(us);
            await _context.SaveChangesAsync();
            return true;
        }
        public async Task<bool> DeleteAllByYearAndMonthAsync(string month, string year)
        {
            var trs = await _context.Transactions.Where(c => c.Date.Contains('.' + month + '.' +  year)).ToListAsync();
            foreach (var t in trs)
            {
                _context.Transactions.Remove(t);
                await _context.SaveChangesAsync();
            }
            return true;
        }
        public async Task<bool> UpdateAsync(TransactionDto transaction)
        {
            
            
            if (transaction == null)
                return false;
            var tr =_context.Transactions.AsNoTracking().FirstOrDefaultAsync(c=>c.Id == transaction.Id);
            if (tr == null) { return false; }

            //Дополняем сущность Транзакция дополнительными сущностями Fuel & User & GasStation
            transaction.Fuel = await _fuel.GetByIdAsync(transaction.FuelId);
            transaction.User = await _user.GetByIdAsync(transaction.UserId);
            transaction.GasStation = await _gs.GetByIdAsync(transaction.GasStationId);
            transaction.Price = transaction.Liters * transaction.Fuel.Price;
            transaction.SavedMoney = transaction.Liters * transaction.User.Loyalty.Coef;


            //Берем транзакцию и пользователя из бд
            var llast = tr.Result.Liters;
            var slast = tr.Result.SavedMoney;


            _context.Transactions.Update(TransactionConverter.Convert(transaction));
            _context.SaveChanges();

            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            var us = await _context.Users.FindAsync(transaction.UserId);
            
            _context.Users.Attach(us);
            //Отменяем результаты транзакции у пользователя
            us.LitersThisMonth -= llast;
            us.SavedMoney -= slast;
            
            //Загружаем пользователю новые результаты транзакции 
            us.LitersThisMonth += transaction.Liters;
            us.SavedMoney += transaction.SavedMoney;
            
            //_context.Users.Update(UserConverter.Convert(us));
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
