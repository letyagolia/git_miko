﻿using Microsoft.EntityFrameworkCore;
using Miko.Core.EF;
using Miko.Data.Converters;
using Miko.Data.Dto;
using Miko.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Core.Repositories
{
    public class FuelRepository: IFuelRepository
    {
        private readonly MicoContext _context;

        public FuelRepository(MicoContext context)
        {
            _context = context;
        }
        public async Task<List<FuelDto>> GetAllAsync()
        {
            return FuelConverter.Convert(await _context.Fuels.ToListAsync());
        }
        public async Task<FuelDto> GetByIdAsync(Guid? id)
        {
            var f = await _context.Fuels.FindAsync(id);
            if (f == null) { return null; }
            return FuelConverter.Convert(f);
        }
        public async Task<FuelDto> GetByTitleAsync(string title)
        {
            var a = await _context.Fuels.FirstOrDefaultAsync(c => c.Title == title);
            if (a == null) { return null; }
            return FuelConverter.Convert(a);
        }
        public async Task<FuelDto> CreateAsync(FuelDto fuel)
        {
            var fu = _context.Fuels.Add(FuelConverter.Convert(fuel));
            await _context.SaveChangesAsync();
            return FuelConverter.Convert(fu.Entity);
        }
        public async Task<bool> DeleteAsync(Guid id)
        {
            var fu = await _context.Fuels.FindAsync(id);
            if (fu == null)
                return false;
            await _context.Transactions.Where(c => c.FuelId == id).ToListAsync();
            _context.Fuels.Remove(fu);
            await _context.SaveChangesAsync();
            return true;
        }
        public async Task<bool> UpdateAsync(FuelDto fuel)
        {
            if (fuel == null)
                return false;
            _context.Fuels.Update(FuelConverter.Convert(fuel));
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
