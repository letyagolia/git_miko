﻿using Microsoft.EntityFrameworkCore;
using Miko.Core.EF;
using Miko.Data.Converters;
using Miko.Data.Dto;
using Miko.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Miko.Core.Repositories
{
    public class GasStationRepository: IGasStationRepository
    {
        private readonly MicoContext _context;

        public GasStationRepository(MicoContext context)
        {
            _context = context;
        }
        public async Task<List<GasStationDto>> GetAllAsync()
        {
            return GasStationConverter.Convert(await _context.GasStations.ToListAsync());
        }
        public async Task<GasStationDto> GetByIdAsync(Guid? id)
        {
            var gs = await _context.GasStations.FindAsync(id);
            if (gs == null) { return null; }
            return GasStationConverter.Convert(gs);
        }
        public async Task<GasStationDto> CreateAsync(GasStationDto gas)
        {
            var gs = _context.GasStations.Add(GasStationConverter.Convert(gas));
            await _context.SaveChangesAsync();
            return GasStationConverter.Convert(gs.Entity);
        }
        public async Task<bool> UpdateAsync(GasStationDto gas)
        {
            if (gas == null)
            {
                return false;
            }
            _context.GasStations.Update(GasStationConverter.Convert(gas));
            await _context.SaveChangesAsync();
            return true;
        }
        public async Task<bool> DeleteAsync(Guid id)
        {
            var gs = await _context.GasStations.FindAsync(id);
            if (gs == null)
                return false;
            await _context.Transactions.Where(c => c.GasStationId == id).ToListAsync();
            _context.GasStations.Remove(gs);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
