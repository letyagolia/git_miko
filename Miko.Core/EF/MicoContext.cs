﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Miko.Data.Enteties;
using System;
using System.Collections.Generic;
using System.Text;

namespace Miko.Core.EF
{
    public class MicoContext: IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Loyalty> Loyalties { get; set; }
        public DbSet<Fuel> Fuels { get; set; }
        public DbSet<GasStation> GasStations { get; set; }
        public DbSet<Imag> Images { get; set; }
        public MicoContext(DbContextOptions<MicoContext> opt) :base(opt)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<IdentityRole<Guid>>().HasData(
                new IdentityRole<Guid>[]
                {
                    new IdentityRole<Guid>
                    {
                        Id = Guid.NewGuid(),
                        Name = "admin",
                        NormalizedName = "ADMIN"
                    },
                    new IdentityRole<Guid>
                    {
                        Id = Guid.NewGuid(),
                        Name = "user",
                        NormalizedName = "USER"
                    }
                }
            );
            base.OnModelCreating(builder);
        }


    }
}
